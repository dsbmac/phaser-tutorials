var Match3 = Match3 || {};

Match3.GameState = {

	init: function () {
		this.NUM_ROWS = 8;
		this.NUM_COLS = 8;
		this.NUM_VARIATIONS = 7;
		this.BLOCK_SIZE = 35;
		this.ANIMATION_TIME = 200;
	},

	create: function () {
		this.background = this.add.sprite(0, 0, 'background');
		this.blocks = this.add.group();
		this.board = new Match3.Board(this, this.NUM_ROWS, this.NUM_COLS, this.NUM_VARIATIONS);
		this.board.consoleLog();
	},

	createBlock: function (x, y, data) {
		var block = this.block.getFirstExists(false);

		if (!block) {
			block = new Match3.Block(this, x, y);
			this.blocks.add(block);
		} else {
			block.reset(x, y, data);
		}

		return block;
	},
	
	

	// //checks the block at xy to see if there is a chain
	// isChain: function (x, y) {
	// 	if (this.isChainInBounds(x, y)) {
	// 		return this.isXChain(x,y) || this.isYChain(x,y);
	// 	} else {
	// 		return false;
	// 	}
	// },

	// isXChain: function (col, row) {
	// 	var blockValue = this.board.grid[row][col];

	// 	//is left block the same
	// 	var isLeftBlockSame = this.board.grid[row][col - 1] === blockValue;

	// 	//is right block the same
	// 	var isRightBlockSame = this.board.grid[row][col + 1] === blockValue;

	// 	return isLeftBlockSame && isRightBlockSame;
	// },

	// isYChain: function (col, row) {
	// 	var blockValue = this.board.grid[row][col];

	// 	//is bottom block the same
	// 	var isBottomBlockSame = this.board.grid[row - 1][col] === blockValue;

	// 	//is right block the same
	// 	var isTopBlockSame = this.board.grid[row + 1][col] === blockValue;

	// 	return isBottomBlockSame && isTopBlockSame;

	// },

	// // returns bool for a given col, row (at center) whether it is in bounds for a chain
	// isChainInBounds: function (x, y) {
	// 	//check column is in bounds
	// 	for (var row = x - 1; row < x + 1; row++) {
	// 		if (row < 0 || row > this.board.grid[0].length) {
	// 			return false;
	// 		}
	// 	}

	// 	//check rows are in bounds
	// 	for (var col = y - 1; col < y + 1; col++) {
	// 		if (col < 0 || col > this.board.grid.length) {
	// 			return false;
	// 		}
	// 	}

	// 	//all tests pass
	// 	return true;
	// }
};

