var Match3 = Match3 || {};

Match3.PreloadState = {
preload: function() {
		 this.preloadBar = this.add.sprite(this.game.world.centerX, this.game.world.centerY, 'bar');
		 this.preloadBar.anchor.setTo(0.5);
		 this.preloadBar.scale.setTo(100, 1);
		 this.load.setPreloadSprite(this.preloadBar);

		 this.load.image('block1', 'assets/images/slice09_09.png');
		 this.load.image('block2', 'assets/images/slice28_28.png');
		 this.load.image('block3', 'assets/images/slice39_39.png');
		 this.load.image('block4', 'assets/images/slice48_48.png');
		 this.load.image('block5', 'assets/images/slice33_33.png');
		 this.load.image('block6', 'assets/images/slice49_49.png');
		 this.load.image('block7', 'assets/images/slice40_40.png');
		 this.load.image('block8', 'assets/images/slice50_50.png');
		 this.load.image('deadblock', 'assets/images/slice03_03.png');
		 this.load.image('background', 'assets/images/background.jpg');
	 },
create: function() {
		this.state.start('Game');
}
};






