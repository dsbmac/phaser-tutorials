var Match3 = Match3 || {};

Match3.Board = function (state, rows, cols, blockVariations) {
	this.state = state;
	this.rows = rows;
	this.cols = cols;
	this.blockVariations = blockVariations;

	this.grid = [];

	var i, j;
	for (i = 0; i < rows; i++) {
		this.grid.push([]);

		for (j = 0; j < cols; j++) {
			this.grid[i].push(0);
		}
	}
	// this is outside the main board	
	this.reserveGrid = [];

	this.RESERVE_ROW = 5;

	for (i = 0; i < this.RESERVE_ROW; i++) {
		this.reserveGrid.push([]);

		for (j = 0; j < cols; j++) {
			this.reserveGrid[i].push(0);
		}
	}

	//initiate  grid
	this.populateGrid();
	this.populateReserveGrid();
};


Match3.Board.prototype.populateGrid = function () {
	var i, j, variation;
	for (i = 0; i < this.rows; i++) {
		for (j = 0; j < this.cols; j++) {
			variation = Math.floor(Math.random() * this.blockVariations) + 1;
			this.grid[i][j] = variation;
		}
	}
};

Match3.Board.prototype.populateReserveGrid = function () {
	var i, j, variation;
	for (i = 0; i < this.RESERVE_ROW; i++) {
		for (j = 0; j < this.cols; j++) {
			variation = Math.floor(Math.random() * this.blockVariations) + 1;
			this.reserveGrid[i][j] = variation;
		}
	}
};


//swap blocks
Match3.Board.prototype.consoleLog = function (source, target) {
	var temp = this.grid[target.row][target.col];
	this.grid[target.row][target.col] = this.grid[source.row][source.col];
	this.grid[source.row][source.col] = temp;
}

Match3.Board.prototype.consoleLog = function () {
	var i, j;
	var prettyString = '';

	for (i = 0; i < this.RESERVE_ROW; i++) {
		prettyString += '\n';

		for (j = 0; j < this.cols; j++) {
			prettyString += ' ' + this.reserveGrid[i][j];
		}
	}

	prettyString += '\n'

	for (j = 0; j < this.cols; j++) {
		prettyString += ' -';
	}

	for (i = 0; i < this.rows; i++) {
		prettyString += '\n';
		for (j = 0; j < this.cols; j++) {
			prettyString += ' ' + this.grid[i][j];
		}
	}

	console.log(prettyString);
};

//swaps candies on user selection
Match3.Board.prototype.swap = function (sourceBlock, targetBlock) {
	var temp = this.grid[targetBlock.row][targetBlock.col];
	this.grid[targetBlock.row][targetBlock.col] = this.grid[sourceBlock.row][sourceBlock.col];
	this.grid[sourceBlock.row][sourceBlock.col] = temp;

	// this.moveBlock(sourceBlock, targetBlock.row, targetBlock.col);		
	// this.moveBlock(targetBlock, sourceBlock.row, sourceBlock.col);

	this.consoleLog();
}

//moves a block to another position
Match3.Board.prototype.moveBlock = function (block, row, col) {
	this.grid[row][col] = block.variation;
}

Match3.Board.prototype.isAdjacent = function (block1, block2) {
	var rowDiff = Math.abs(block1.row - block2.row);
	var colDiff = Math.abs(block1.col - block2.col);

	return (rowDiff === 1 && colDiff === 0) || (rowDiff === 0 && colDiff === 1);

}

Match3.Board.prototype.isChained = function (block) {
	var variation = this.grid[block.row][block.col];
	var row = block.row;
	var col = block.col;

	//left
	if (variation === this.grid[row][col - 1] && variation === this.grid[row][col - 2]) {
		return true;
	}

	//right
	if (variation === this.grid[row][col + 1] && variation === this.grid[row][col + 2]) {
		return true;
	}

	//up
	if (this.grid[row - 2]) {
		if (variation === this.grid[row - 1][col] && variation === this.grid[row - 2][col]) {
			return true;
		}
	}

	//down
	if (this.grid[row + 2]) {
		if (variation === this.grid[row + 1][col] && variation === this.grid[row + 2][col]) {
			return true;
		}
	}

	//center-horizontal
	if (variation === this.grid[row][col - 1] && variation === this.grid[row][col + 1]) {
		return true;
	}

	//center-vertical
	if (this.grid[row + 1] && this.grid[row - 1]) {
		if (variation === this.grid[row + 1][col] && variation === this.grid[row - 1][col]) {
			return true;
		}
	}

	// all failed
	return false;
};

Match3.Board.prototype.findAllChains = function () {
	var chained = [];
	var i, j;

	for (i = 0; i < this.rows; i++) {
		for (j = 0; j < this.cols; j++) {
			if (this.isChained({ row: i, col: j })) {
				chained.push({ row: i, col: j });
			}
		}
	}


	return chained;
};

//clear all the chains
Match3.Board.prototype.clearChains = function () {

	var chainedBlocks = this.findAllChains();


	chainedBlocks.forEach(function (block) {
		this.grid[block.row][block.col] = 0;

	}, this);

	this.consoleLog();

};
