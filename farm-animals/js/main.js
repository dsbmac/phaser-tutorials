var game = new Phaser.Game(640, 360, Phaser.AUTO);

var animals;
var currentAnimal;

//animal group
var animalData = [
    { key: 'chicken', text: 'CHICKEN', scale: 0.15 },
    { key: 'horse', text: 'HORSE', scale: 0.5 },
    { key: 'cow', text: 'COW', scale: 0.38 }
]

var GameState = {
    preload: function () {
        this.load.image('background', 'assets/images/farm-bg.png');
        this.load.image('horse', 'assets/images/horse.png');
        this.load.image('chicken', 'assets/images/chicken2.png');
        game.load.image('cow', 'assets/images/cow.png');
        game.load.image('arrow', 'assets/images/arrow.png');
    },

    create: function () {
        //page scaling
        this.scale.scaleMode = Phaser.ScaleManager.SHOW_ALL;
        this.scale.pageAlignHorizontally = true;
        this.scale.pageAlignVertically = true;


        // add sprites

        //background
        background = game.add.sprite(0, 0, 'background');
        background.height = game.height;
        background.width = game.width;

        //right arrow
        rightArrow = game.add.sprite(580, game.world.centerY, 'arrow');
        rightArrow.anchor.setTo(0.5, 0.5);
        rightArrow.scale.setTo(0.15);
        rightArrow.inputEnabled = true;
        rightArrow.events.onInputDown.add(this.changeAnimal, this);

        //left arrow
        leftArrow = game.add.sprite(60, game.world.centerY, 'arrow');
        leftArrow.anchor.setTo(0.5, 0.5);
        leftArrow.scale.setTo(-0.15);
        leftArrow.inputEnabled = true;
        leftArrow.events.onInputDown.add(this.changeAnimal, this);

        //create animals
        animals = game.add.group();

        var animal;
        var self = this;
        for (var i = 0; i < animalData.length; i++) {
            var element = animalData[i];
            //  This creates a new Phaser.Sprite instance within the group
            // -1000 value to hide off screen
            animal = animals.create(game.world.centerX, game.world.centerY, element.key);
            animal.visible = false;
            animal.scale.setTo(element.scale);
            animal.anchor.setTo(0.5);
            animal.customParams = { text: element.text };
            animal.inputEnabled = true;
            animal.input.pixelPerfectClick = true;
            animal.events.onInputDown.add(self.animateAnimal, self);
        }

        // show the initial animal
        this.changeAnimal();

        //flip horizontally
        // this.horse.scale.setTo(-1 * this.horse.scale.x, 1 * this.horse.scale.y);


        //rotation
        // this.chicken.angle = 45;
    },

    update: function () {
    },

    rotateAnimal: function (sprite, event) {
        if (sprite.key === 'chicken') {
            sprite.angle += 1;
        }
    },

    changeAnimal: function (sprite, event) {
        if (currentAnimal) {
            currentAnimal.visible = false;
        }

        //show the current animal
        //check if the sprite is flipped to determine direction
        if (sprite && sprite.scale.x < 0) {
            currentAnimal = animals.previous();
        } else {

            currentAnimal = animals.next();
        }
        
        currentAnimal.visible = true;
    },

    animateAnimal: function (sprite, event) {
        console.log('animating animal...');
    }
};

game.state.add('GameState', GameState);
game.state.start('GameState');