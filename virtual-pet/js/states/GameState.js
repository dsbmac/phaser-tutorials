var GameState = {


    create: function () {
        this.background = this.game.add.sprite(0, 0, 'backyard');
        this.pet = this.game.add.sprite(100, 400, 'pet');

        //custom properties
        this.pet.customParams = { health: 100, fun: 100 };
        this.apple = this.game.add.sprite(72, 570, 'apple');
        this.candy = this.game.add.sprite(144, 570, 'candy');
        this.toy = this.game.add.sprite(216, 570, 'toy');
        this.rotate = this.game.add.sprite(288, 570, 'rotate');
    },

    update: function () {
        if (this.pet.customParams.health <= 0 || this.pet.customParams.fun <= 0) {
            this.pet.frame = 4;
            this.uiBlocked = true;

            this.game.time.events.add(2000, this.gameOver, this);
        }
        
        
    },
    
    gameOver: function() {
        this.state.start('HomeState', true, false, 'Game Over');
    }
};