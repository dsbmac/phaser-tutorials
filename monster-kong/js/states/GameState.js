var GameState = {
    init: function () {
        this.RUNNING_SPEED = 180;
        this.JUMPING_SPEED = 550;
        this.cursors = this.game.input.keyboard.createCursorKeys();

        //size
        this.game.world.setBounds(0, 0, 360, 700);

    },

    create: function () {
        this.ground = this.game.add.sprite(0, 700, 'ground');
        this.game.physics.arcade.enable(this.ground);
        this.ground.body.allowGravity = false;
        this.ground.body.immovable = true;

        //parse level file
        this.levelData = JSON.parse(this.game.cache.getText('level'));

        this.platforms = this.add.group();
        this.platforms.enableBody = true;

        this.levelData.platformData.forEach(function (elem) {
            this.platforms.create(elem.x, elem.y, 'platform')
        }, this);

        this.platforms.setAll('body.immovable', true);
        this.platforms.setAll('body.allowGravity', false);

        // this.platform = this.game.add.sprite(0, 300, 'platform');
        // this.game.physics.arcade.enable(this.platform);
        // this.platform.body.allowGravity = false;
        // this.platform.body.immovable = true;

        this.player = this.add.sprite(this.levelData.playerStart.x, this.levelData.playerStart.y, 'player', 1);
        this.player.anchor.setTo(0.5);
        this.player.animations.add('walking', [1, 2, 3, 4], 6, true);
        this.game.physics.arcade.enable(this.player);
        this.player.customParams = {};
        this.game.camera.follow(this.player);

        this.createOnScreenControls();
    },

    update: function () {
        //able to walk on
        this.game.physics.arcade.collide(this.player, this.ground);
        this.game.physics.arcade.collide(this.player, this.platforms);

        //user controls
        this.player.body.velocity.x = 0;

        if (this.cursors.left.isDown || this.player.customParams.isMovingLeft) {
            this.player.body.velocity.x = -this.RUNNING_SPEED;
            this.player.play('walking');
        } else if (this.cursors.right.isDown || this.player.customParams.isMovingRight) {
            this.player.body.velocity.x = this.RUNNING_SPEED;
        } else {
            this.player.animations.stop();
            this.player.frame = 0;
        } 
        
        if ((this.cursors.up.isDown || this.player.customParams.mustJump ) && this.player.body.touching.down) {
            this.player.body.velocity.y = -this.JUMPING_SPEED;
        }
    },

    createOnScreenControls: function () {
        // this.leftArrow = this.add.button(20, 535, 'arrowButton');
    }
};