var BootState = {
    init: function () {
        this.scale.scaleMode = Phaser.ScaleManager.SHOW_ALL;
        this.scale.pageAlignHorizontally = true;
        this.scale.pageAlignVertically = true;
        
        this.game.physics.startSystem(Phaser.Physics.ARCADE);
        this.game.physics.arcade.gravity.y = 1000;
        
    },

    preload: function() {
        this.load.image('preloadBar', 'assets/images/loading_bar.png');
        this.load.image('logo', 'assets/images/rubber_duck.png');
    },
    
    create: function() {
        this.game.stage.backgroundColor = '#fff';
        
        this.state.start('PreloadState');
    }
};
