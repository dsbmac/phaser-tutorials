var PreloadState = {
    preload: function () {
                
        this.load.image('ground', 'assets/images/ground.png');
        this.load.image('platform', 'assets/images/platform.png');

        this.load.spritesheet('player', '/assets/images/player_spritesheet2.png',64,64);
        
        //load level file
        this.load.text('level', 'assets/data/level.json');
    },
    
    
    create: function() {
        
        this.state.start('GameState');
    }
};
